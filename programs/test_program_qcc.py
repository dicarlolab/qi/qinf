from qinf import Program


def metal_insulator_transition(num_trotter_steps=20):
    program = Program("metal_insulator_transition")
    q0 = 'Q2'
    q1 = 'Q0'
    for idx_t_step in range(0, num_trotter_steps):
        k = program.new_kernel('unoptimized_{}'.format(idx_t_step))

        k.prepz()
        k.rx(q0, 180)

        # k.gate('rx180', [q0])
        for itr in range(0, (idx_t_step+1)):
            k.rx(q1, -90)
            k.cz(q0, q1)
            k.ry(q1, 90)
            k.rz(q0, -68.4)
            k.rz(q1, 14.4)
            k.rotate(q0, phi=90, theta=23.876)

            k.cz(q0, q1)
            k.rx(q0, 14.4)
            k.cz(q0, q1)
            k.rx(q0, 14.4)
            k.ry(q1, -90)
            k.cz(q0, q1)
            k.ry(q1, 90)
            k.rz(q1, -18)
            k.rx(q0, 120.6)
            k.rx(q1, -41.4)
        k.measure()
    program.compile()
    return program


metal_insulator_transition(num_trotter_steps=10)
