"""
Contains functions to simulate qinf wrapped OpenQL programs using quantumsim.
"""
import numpy as np
from quantumsim.qasm import ConfigurableParser
from quantumsim.sparsedm import SparseDM


def simulate_two_qubit_program(program):
    """
    Simulate an QInf OpenQL program using quantumsim.

    Parameters
    ----------
    program : qinf.Program
        QInf program instance that has been compiled.

    Returns
    -------
    results: array_like
        outcome probabilities of experiment.
        columns are: p(QL), p(QR), parity(QL, QR)
        rows correspond to distinct measurements
    """
    parser = ConfigurableParser(program.ccl_config_fp)
    circuits = parser.parse(program.qasm_fp, toposort=False)

    density_matrices = []
    results = []

    for c in circuits:
        qubits = sorted(c.get_qubit_names())
        state = SparseDM(qubits)
        for q in qubits:
            state.ensure_dense(q)
        c.apply_to(state, apply_all_pending=True)
        density_matrices.append(state.full_dm.to_array())

        diag = state.full_dm.get_diag()
        parity = diag[[0, 3]].sum()
        p0 = diag[[1, 3]].sum()
        p1 = diag[[2, 3]].sum()
        results.append((p0, p1, parity))
    results = np.array(results)
    return results
