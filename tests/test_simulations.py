from pytest import approx, warns
from qinf import Program
from qinf.simulate import simulate_two_qubit_program


def test_simulate_simple_program():
    # test output for correctness as well as ability to compile
    p = Program("simple")
    k = p.new_kernel('k0')
    k.prepz()
    k.measure()

    k = p.new_kernel('k1')
    k.prepz()
    k.rx('QL', 90)
    k.measure()

    k = p.new_kernel('k2')
    k.prepz()
    k.rx('QL', 180)
    k.rx('QR', 180)
    k.measure()

    k = p.new_kernel('k3')
    k.prepz()
    k.rx('QR', 180)
    k.measure()
    p.compile()

    with warns(UserWarning):
        results = simulate_two_qubit_program(p)
    assert results[:, 0] == approx([0, .5, 1, 0])
    assert results[:, 1] == approx([0, 0, 1, 1])
    assert results[:, 2] == approx([1, 0.5, 1, 0])

